from aLiYun import aLiYun
import log
import modem
import _thread
import checkNet
import utime
import usys
import ujson
import urandom
import modem
import net
import cellLocator
from misc import Power

from gnss import GnssGetData

gnssHandle = GnssGetData(1, 9600, 8, 0, 1, 0)
IMEI = modem.getDevImei()


global jsonMsg
current = 0.0
currentVoltage = 0.0
batteryPercentage = 0
speed = 0.0
mileage = 0
errInfo = ''

longitude = 0.0
latitude = 0.0
altitude = 0.0

Longitude_d = {"Longitude": 0}
Latitude_d = {"Latitude": 0}
Altitude = {"Altitude": 0}
location_d = {"CoordinateSystem": 1}

location_d.update(Longitude_d)
location_d.update(Latitude_d)
location_d.update(location_d)
geo_d = {'GeoLocation': location_d}


IMEI_d = {'IMEI': IMEI}
BatteryCurrent_d = {'BatteryCurrent': 5.1}
BatteryVoltage_d = {'BatteryVoltage': 73.2}
SideStand_d = {'SideStand': 0}
BrakeStatus_d = {'BrakeStatus': 0}
GearInfo_d = {'GearInfo': 1}
MotorRPM_d = {'MotorRPM': 124}
ThrottlePercent_d = {'ThrottlePercent': 55}
MotorTemperature_d = {'MotorTemperature': 50}
ControllerTemperature_d = {'ControllerTemperature': 30}
ErrorInfo_d = {'ErrorInfo': 'No Err'}
BatteryPercentage_d = {'BatteryPercentage': 0}
ScooterInfo_d = {'IMEI': IMEI}

ScooterInfo_d.update(BatteryCurrent_d)
ScooterInfo_d.update(BatteryVoltage_d)
ScooterInfo_d.update(SideStand_d)
ScooterInfo_d.update(BrakeStatus_d)
ScooterInfo_d.update(GearInfo_d)
ScooterInfo_d.update(MotorRPM_d)
ScooterInfo_d.update(ThrottlePercent_d)
ScooterInfo_d.update(MotorTemperature_d)
ScooterInfo_d.update(ControllerTemperature_d)
ScooterInfo_d.update(ErrorInfo_d)
ScooterInfo_d.update(IMEI_d)
ScooterInfo_d.update(BatteryPercentage_d)


params_d = {'params': ScooterInfo_d}


sub = "/sys/a1GltmDmjk0/test/thing/service/property/set"
sub1 = '/sys/a1GltmDmjk0/test/thing/event/property/post_reply'
pub = "/sys/a1GltmDmjk0/test/thing/event/property/post"
sub = pub
msgID = 0
jsonMsg = {"clientId": 'test', "id": 123,
           "method": "thing.event.property.post"}
jsonMsg.update(params_d)


def gnssThread():
    global gnssHandle
    while (True):
        utime.sleep(10)
        


state = 1


def ali_sub_cb(topic, msg):
    global state
    state += 1


msgID = 0
current = 0
voltage = 0


def aLiYunThread():
    while (True):
        global msgID
        global current
        global voltage
        msgID += 1
        jsonMsg['id'] = msgID
        current = urandom.randint(1, 50)
        voltage = urandom.randint(60, 90)
        BatteryCurrent_d['BatteryCurrent'] = current
        BatteryVoltage_d['BatteryVoltage'] = voltage
        ScooterInfo_d.update(BatteryCurrent_d)
        ScooterInfo_d.update(BatteryVoltage_d)
        ali.publish(pub, ujson.dumps(jsonMsg))
        utime.sleep(10)


ali = aLiYun("a1GltmDmjk0", None, "test", "eecd2357a9e9bb7c1f44f08061e413ae",
             "a1GltmDmjk0.iot-as-mqtt.cn-shanghai.aliyuncs.com")


def ali_service():
    print("Service")
    _thread.start_new_thread(gnssThread, ())


if __name__ == '__main__':
    PROJECT_NAME = "QuecPython"
    PROJECT_VERSION = "1.0.0"
    apn = net.getApn(0)
    print(apn)

    checknet = checkNet.CheckNetwork(PROJECT_NAME, PROJECT_VERSION)
    checknet.poweron_print_once()
    try:
        checknet.wait_network_connected()
    except BaseException:
        print('Not Net, Resatrting...')
        utime.sleep_ms(200)
        Power.powerRestart()

    ali_service()
