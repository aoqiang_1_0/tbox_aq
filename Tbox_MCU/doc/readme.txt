07/04/2023
1.send soc to can1 and can2
2.send bms id to ec600 in json format.


07/05/2023：
1.send CAN message with CANID 0x190, data[0]=1 to enable controller by default.

08/01/2023:
1.sleep if 12v is not exist for 180 seconds.
2.support 12v off->on to wake up tbox
3.support read gear info and send to cloud
4.optimize read bms info 


08/12/2023:
1.send controller enable/disble cmd / SOC info on both CAN1 and CAN2
2.add receive controller enable/disable info from ec600 through uart4