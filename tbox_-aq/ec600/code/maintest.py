from gnss import GnssGetData
from umqtt import MQTTClient
from aLiYun import aLiYun
import modem
import _thread
import checkNet
import utime
import log
import net
import ujson
from misc import Power
from machine import UART
import ntptime
import dataCall
import cellLocator

log.basicConfig(level=log.INFO)
aliYun_log = log.getLogger("test")


gnssHandle = GnssGetData(1, 9600, 8, 0, 1, 0)


sub = "/sys/a1GltmDmjk0/test/thing/service/property/set"
sub1 = '/sys/a1GltmDmjk0/test/thing/event/property/post_reply'
pub = "/sys/a1GltmDmjk0/test/thing/event/property/post"
sub = pub

global jsonMsg
current = 0.0
currentVoltage = 0.0
batteryPercentage = 0
speed = 0.0
mileage = 0
errInfo = ''

longitude = 0.0
latitude = 0.0
altitude = 0.0

mcuMsg = {'a': 0}
params_d = {'params': mcuMsg}

msgID = 0


Longitude = {'Longitude': 0}
Lon_direction = {'Lon_direction': 'E'}
Latitude = {'Latitude': 0}
Lat_direction = {'Lat_direction': 'N'}
Latitude.update(Longitude)
Latitude.update(Lon_direction)
Latitude.update(Lat_direction)

geolocation = {'GPS Coordinates': Latitude}


jsonMsg = {"clientId": 'test', "id": 123,
           "method": "thing.event.property.post"}
jsonMsg.update(params_d)


def gnssThread():
    global gnssHandle
    while (True):
        utime.sleep(5)
        gnssHandle.read_gnss_data(1, 1)
        print(gnssHandle.getLocation())
        if (gnssHandle.getLocation() != -1):
            print('locate ok')
            a, b, c, d = gnssHandle.getLocation()
            print(a, b, c, d)
            Longitude['Longitude'] = a
            Lon_direction['Lon_direction'] = b
            Latitude['Latitude'] = c
            Lat_direction['Lat_direction'] = d
        else:
            print("use cell locator instead")
            a, b, c = cellLocator.getLocation(
                "www.queclocator.com", 80, "9M83X4298Iv91y71", 8, 1)
            Longitude['Longitude'] = a
            Latitude['Latitude'] = c
            print(a, b, c)
            utime.sleep(20)


uart = UART(UART.UART2, 115200, 8, 0, 1, 0)

uart_ack = bytes([0x55, 0x00])

global tmpPackLen


def nw_cb(args):
    nw_stat = args[1]
    if nw_stat == 1:
        utime.sleep(2)
        print("net connect,sub topic")
        c.subscribe(SUB_TOPIC)
    else:
        print("net down")


cmd = bytearray([0xdd, 0x00, 0xee])


def send_controller_control_cmd(command):
    cmd[1] = command
    uart.write(cmd)


def uart_recv_thread():
    while (True):
        utime.sleep_ms(1000)
        while (True):
            if (uart.any() >= 1):
                data = uart.read(1)
                if (data[0] == 0xdd):
                    break
                else:
                    continue
            else:
                continue
        utime.sleep_ms(5)
        len = uart.any()
        print(len)
        data = uart.read(len)
        print(data)
        tmpPackLen = 0
        for eachbyte in data:
            tmpPackLen = tmpPackLen+1
            if (eachbyte == 0xee):
                break
        if (tmpPackLen <= len):
            data = data[0:tmpPackLen-1]
        else:
            continue

        mcuMsg.update(ujson.loads(data))
        params_d.update(mcuMsg)
        print("pack ok")


def update_jsonMsg():
    print('update msg')
    date = utime.localtime()
    if 'a' in mcuMsg:
        mcuMsg.pop('a')
    mcuMsg["ctime"] = utime.mktime(date)
    jsonMsg.update(params_d)


def sub_cb(topic, msg):
    global c
    print(
        "Subscribe Recv: Topic={},Msg={}".format(
            topic.decode(),
            msg.decode()))


def MQTT_PUB_ALI():
    global ali
    while True:
        utime.sleep(3)
        send_controller_control_cmd(1)
        update_jsonMsg()
        print(jsonMsg)
        if ali is not None:
            ali.publish(pub, ujson.dumps(jsonMsg))
        else:
            print("ali is null")
            utime.sleep(10)


ali = aLiYun("a1GltmDmjk0", None, "test", "eecd2357a9e9bb7c1f44f08061e413ae",
             "a1GltmDmjk0.iot-as-mqtt.cn-shanghai.aliyuncs.com")


def ali_sub_cb(topic, msg):
    global state
    state += 1
    print("Subscribe Recv: Topic={},Msg={}".format(topic.decode(), msg.decode()))


def ali_service():
    global ali
    ali.setMqtt("123", clean_session=False, keepAlive=300, reconn=True)
    ali.setCallback(ali_sub_cb)
    ali.subscribe(sub, 0)
    ali.start()


def GetDevImei():
    global IMEI
    IMEI = modem.getDevImei()
    jsonMsg['IMEI'] = IMEI
    print(IMEI)


def main():
    GetDevImei()
    ali_service()

    _thread.start_new_thread(MQTT_PUB_ALI, ())
    _thread.start_new_thread(uart_recv_thread, ())
    _thread.start_new_thread(gnssThread, ())


if __name__ == '__main__':
    PROJECT_NAME = "QuecPython"
    PROJECT_VERSION = "1.0.0"

    apn = net.getApn(0)
    print(apn)

    checknet = checkNet.CheckNetwork(PROJECT_NAME, PROJECT_VERSION)
    checknet.poweron_print_once()
    stagecode, subcode = checknet.wait_network_connected(30)
    if subcode == 1 and stagecode == 3:
        print("net ok")
        dataCall.setCallback(nw_cb)
        main()
    else:
        print('stagecode = {}, subcode = {}'.format(stagecode, subcode))
        print('Not Net, Resatrting after 10second...')
        utime.sleep_ms(1000)
        Power.powerRestart()
